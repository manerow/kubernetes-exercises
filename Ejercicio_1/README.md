## Ejercicio 1

Para desplegar el pod en nuestro nodo de kubernetes ejecutamos: 
```
$ kubectl create -f nginx-pod.yaml
```
#### 1. ¿Cómo puedo obtener las últimas 10 líneas de la salida estándar (logs generados por la aplicación)? <br>
Si deseamos obtener los logs de nuestro pod, la api de kubernetes nos proporciona el siguiente comando. Con el flag "tail" podemos indicarle el numero de las últimas lineas que queremos visualizar de ese log.
  ```
  $ kubectl logs <nombre_pod> --tail=10
  ```
#### 2. ¿Cómo podría obtener la IP interna del pod? Aporta capturas para indicar el proceso que seguirías. <br>
Para obtener la IP interna del pod podemos hacerlo mediante distintos comandos de kubernetes.
- Podemos obtener la descripcion del pod y buscar el parámetro "IP" mediante el comando:
  ```
  $ kubectl describe pod <nombre_pod>
  ```
- Por otro lado, podemos ejecutar el comando:
  ```
  $ kubectl get pods -o wide
  ```
  Mediante la opción "-o wide" se añade información adicional al output mostrandonos una columna con la IP interna de nuestros pods.<br><br>
- Otra opcion sería entrar dentro del pod y ejecutar un comando en su shell para que nos diga su propia IP.
  ``` 
  $ kubectl exec -it <nombre_pod> bash 
  ```
  Una vez dentro ejecutamos: 
  ```
  $ hostname -I
  ```
#### 3. ¿Qué comando utilizarías para entrar dentro del pod? <br>
Para entrar dentro del pod usaria el comando "exec" de la api de kubernetes. Para atachar su shell a la de mi host, usaria el flag "-it". Finalmente le pasaría como argumento el nombre de la shell que deseo ejecutar dentro del pod. En este caso es "bash".
  ```
  $ kubectl exec -it <nombre_pod> -- bash
  ```
#### 4. Necesitas visualizar el contenido que expone NGINX, ¿qué acciones debes llevar a cabo? <br>
- Podemos mapear el puerto que expone el contenedor con un puerto de nuestro host, de este modo podremos acceder al contenido que expone el pod mediante la url localhost:<num_puerto>. Para ello, deberemos ejecutar el comando:
  ```
  $ kubectl port-forward <nombre_pod> <num_puerto>:80
  ```
- Podemos también consultar que contiene analizando el codigo html. Para hacerlo debemos ejecutar el comando: 
  ```
  $ kubectl exec -it <nombre_pod> -- cat /usr/share/nginx/html/index.html
  ```
#### 5. Indica la calidad de servicio (QoS) establecida en el pod que acabas de crear. ¿Qué lo has mirado? <br>
  
  Para mirar la clase de calidad de servicio que Kubernetes ha asignado a nuestro pod podemos ejecutar el comando:
  ``` 
  $ kubectl describe pod <nombre_pod>
  ```
  Y buscar en la respuesta la Qos Class. <br><br>
  En este caso la QoS Class es 'Guaranteed'. Esto se debe a que en la declaración de nuestro pod cumplimos ciertos requerimientos que establece Kubernetes para asignar dicha clase de Quality of Service. Estos requerimientos consisten en:
  - Cada contenedor del pod tiene un memory limit y un memory request.
  - El memory limit de cada contenedor del pod es igual al memory request.
  - Cada contenedor del pod tiene un CPU Limit y un CPU request.
  - El CPU limit de cada contenedor del pod es igual al CPU request.
