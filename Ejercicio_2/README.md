## Ejercicio 2

#### 1. Debe tener 3 replicas <br>
  Con el objetivo de crear un objeto ReplicaSet que despliegue 3 replicas de nuestro pod de nginx, deberemos indicarle en el manifiesto, el parámetro spec.replicas = 3.

  Para crear el ReplicaSet ejecutamos:
  ```
  $ kubectl apply -f nginx-replicaset.yaml
  ```
 ![ReplicaSet 3 replicas](.bin/img/ex_2-01.png)
#### 2. ¿Cúal sería el comando que utilizarías para escalar el número de replicas a 10? <br>
- Si quisieramos hacerlo de modo imperativo, sería con el comando: 
  ```
  $ kubectl scale --replicas=10 rs/nginx-replicaset 
  ```
- Por el contrario, si quisieramos mantener un registro de cambios y hacerlo de modo declarativo, deberíamos modificar el parámetro "spec.replicas" del manifiesto del replicaset a 10. Posteriormente deberemos ejecutar el comando: 
  ```
  $ kubectl apply -f <path_manifiesto>
  ```
![ReplicaSet 10 replicas](.bin/img/ex_2-02.png)

#### 3. Si necesito tener una replica en cada uno de los nodos de Kubernetes, ¿qué objeto se adaptaría mejor? <br>
  El objeto que mejor se adaptaría seria el DaemonSet, puesto que su funcion es precisamente mantener una replica de cierto pod en cada uno de los nodos de Kubernetes.