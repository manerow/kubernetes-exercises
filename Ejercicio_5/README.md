## Ejercicio 5 <br>
#### Diseña una estrategia de despliegue que se base en ”Blue Green”. Podéis utilizar la imagen del ejercicio 1. <br>

En primer lugar empiezo con el deploy de la primera version de la aplicación. Para ello ejecuto el comando: <br>
```
$ kubectl apply -f deployment-v1.yaml
```
A continuación lanzamos el servicio con el selector apuntando a la version v1.0.0 que acabamos de desplegar. <br>
```
$ kubectl apply -f service.yaml
```
Ahora los usuarios disponen de acceso a la primera version. Podemos ver que los pods de la primera version se estan ejecutando. <br>
```
$ kubectl get pods -w
```
En este punto decidimos lanzar la segunda version de la aplicación mediante la estrategia Blue / Green. Para ello será necesario ejecutar el deployment de la version 2. Aunque deseamos que el trafico siga dirigido a nuestra version 1. Por lo tanto, vamos a hacer que convivan las dos versiones y, de momento, no vamos a modificar el servicio. <br>
```
$ kubectl apply -f deployment-v2.yaml
```
Ahora tendríamos ambas versiones de la aplicación en ejecucion. El trafico a través de la url <cluster_ip>:<node_port> iria dirigido a la version v1. Mientrastanto podríamos testear la nueva version sin pasar por el servicio que acceden los usuarios, ya sea con un servicio auxiliar que apunte a la nueva version y lo usemos solamente para testear o mapeando el puerto de uno de los pods de la nueva version con un puerto de nuestro host (Como en el Ejercicio 1, pregunta 4). <br>

Una vez testeada la nueva version, nos disponemos a redirigir el tráfico hacia ella. Para ello, debemos modificar el selector del servicio por el de la version v2.0.0. <br>

  - Para modificar el selector mediante un comando imperativo, ejecutamos: <br>
  ```
  $ kubectl patch service nginx-service -p '{"spec":{"selector":{"version":"v2.0.0"}}}'
  ```
  - Para modificar el selector de manera declarativa, modificamos en el fichero service.yaml. el parámetro "selector.version" indicandole la version "v2.0.0". Posteriormente ejecutamos el comando:
  ```
  $ kubectl apply -f service.yaml
  ```
Ahora, el servicio por el que acceden los usuarios ya no apunta a la version v1.0.0 sino a la v2.0.0. Por lo tanto, el trafico de los usuarios ha cambiado de la version v1.0.0 a la version v2.0.0
