## Ejercicio 4

#### 1. Despliega una nueva versión de tu nuevo servicio mediante la técnica “recreate” <br>
  Para la ejecución de un deployment de tipo recreate:<br>
  1. Lanzamos el deployment en kubernetes mediante:<br>
  ```
  $ kubectl apply -f deployment-recreate.yaml
  ```
  2. Modificamos el deployment para forzar un re-despliegue con la estrategia "Recreate".<br>
  ```
  $ kubectl set image deployment/<nombre_deployment> <nombre-container>=nginx:1.16.1 
  ```
  A la vez observamos los cambios que tienen lugar en los pods del cluster. <br>
  ```
  $ kubectl get pods -w
  ```
  ![Pods durante deployment Recreate](.bin/img/ex_4-01.png) <br>
  El redespliegue de los nuevos pods con la imagen nginx:1.16.1 se ha efectuado mediante la estrategia Recreate. Primero se ha terminando la ejecucion de los pods que estaban activos, con la version nginx:1.19.4, y luego se ha lanzado los pods con la version de nginx 1.16.1. <br>
#### 2. Despliega una nueva versión haciendo “rollout deployment” <br>
  Para la ejecución de un deployment de tipo Rollout deployment:<br>
  1. Lanzamos el deployment en kubernetes mediante:<br>
  ```
  $ kubectl apply -f deployment-rollingupdate.yaml
  ```
  2. Modificamos el deployment para forzar un re-despliegue con la estrategia "Rollout deployment" <br>
  ```
  $ kubectl set image deployment/<nombre_deployment> <nombre-container>=nginx:1.16.1 
  ```
  A la vez observamos los cambios que tienen lugar en los pods del cluster. <br>
  ```
  $ kubectl get pods -w
  ```
  ![Pods durante deployment Rolling Update](.bin/img/ex_4-02.png) <br>
  El redespliegue de los nuevos pods con la imagen nginx:1.16.1 se ha efectuado mediante la estrategia Rolling Update. A medida que los pods con la version de nginx 1.16.1 se creaban y llegaban al estado de 'Running', los pods con la imagen antigua se iban destruyendo. <br>

#### 3. Realiza un rollback a la versión generada previamente <br>
  Partiendo del punto en el que nos hemos quedado en el ejercicio anterior, tenemos un Deployment con estrategia "Rollout Deployment", con 3 replicas y con la imagen nginx 1.16.1. Ahora queremos volver a implantar la imagen de nginx 1.19.4 que teníamos al punto 1 del apartado 2. Para ello, procedo de la siguiente forma:<br>
  1. Visualizo el historial de deployments mediante el comando:<br>
  ```
  $ kubectl rollout history deployment/<nombre_deployment>
  ```
  ![Rollout history](.bin/img/ex_4-03.png) <br>

  2. Seleccionamos la revision hacia la que deseamos hacer el rollback. En este caso es la 1. Puesto que nos permite volver al punto de partida. <br>
  4. Para ejecutar el rollback del deployment lanzo el comando: <br>
  ```
  $ kubectl rollout undo deployment/<nombre_deployment> --to-revision=1
  ```
