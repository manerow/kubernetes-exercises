## Ejercicio 3

#### 1. Exponiendo el servicio hacia el exterior (crea service1.yaml) <br>
Mediante la declaración de un servicio como LoadBalancer exponemos el servicio al exterior del cluster con una única IP. <br>
Para ello es necesario haber desplegado el servicio en un Cloud Provider (GCP, AWS, etc.), puesto que éste, por detrás, se va a encargar de asignarnos una IP Pública y un balanceador de carga al servicio que hayamos declarado como LoadBalancer. <br>

Para crear el servicio ejecuto el comando:
  ```
  $ kubectl apply -f service1.yaml
  ```
#### 2. De forma interna, sin acceso desde el exterior (crea service2.yaml) <br>
Para evitar exponer nuestros servicios al exterior y a su vez garantizar que es accessible mediante una comunicación interna dentro del cluster, se debe crear el servicio como ClusterIP. <br>
De esta forma se puede acceder al servicio únicamente desde dentro del propio cluster.<br>

Para crear el servicio ejecuto el comando:
  ``` 
  $ kubectl apply -f service2.yaml
  ```
#### 3. Abriendo un puerto especifico de la VM (crea service3.yaml) <br>
Si queremos exponer nuestra aplicación a través de un puerto específico del nodo del cluster en el que se encuentra, deberemos crear un servicio de tipo NodePort. Este servicio requiere de asignar un puerto en el rango 30000-32768 como puerta de enlace al nodo del cluster en el que se haya creado dicho servicio. <br>
En este caso el NodePort asignado es el 30001/TCP del cluster mediante el parámetro del manifiesto "spec.ports.nodePort".<br>
Para acceder desde el exterior deberemos consultar primero la ip del nodo. Para ello, en minikube, podemos hacerlo de distintas formas:
- Ejecutando el comando 
  ```
  $ minikube ip 
  ```
  Una vez obtenida la ip del cluster accedemos al servicio mediante la URL <ip_minikube>:<node_port> <br>
- Ejecutamndo el comando
  ```
  $ minikube service --url <nombre_servicio>
  ```
Para crear el servicio ejecuto el comando: 
  ```
  $ kubectl apply -f service3.yaml
  ```
